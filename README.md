# Go-Inetmap


## Synopsis

Generate a Map of the Internet (/8 networks) plotted over a Hilbert curve.  
Based on [https://www.xkcd.com/195/](https://www.xkcd.com/195/)  
![XKCD: Map of the Internet Comic](https://imgs.xkcd.com/comics/map_of_the_internet.jpg)

## Installation

```
go get -u gitlab.com/uxbh/go-inetmap/
```


## Contributors

Written by Unixblackhole


## License

See [Licence](Licence.md)

