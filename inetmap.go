package inetmap

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"github.com/google/hilbert"
	colorful "github.com/lucasb-eyer/go-colorful"
)

var eightBlock map[int]netBlock

type mapTheme uint8

//Theme Colors for the map
const (
	Bluey mapTheme = iota
	Greeny
	Browny
	White
)

//FontLocation needs to be set to the path of your chosen font.
var FontLocation string

type netMap []NetCell

type mapColor struct {
	Whole bool
	colorful.Color
	Theme mapTheme
}

// Block contains the network map data. Including network names, cell colors and extra data.
var Block netMap

// MapColor sets the whole (or parts of the) map to single color
var MapColor mapColor

// netMapImg contains the image data to be populated by netmap.Block data.
var netMapImg struct {
	*freetype.Context
	*image.RGBA
}

//NetCell contains data for each cell of the network map.
type NetCell struct {
	Label string
	Name  string
	Info  string
	X, Y  int
	Pos   int
	Fill  colorful.Color
	Cell  image.Rectangle
}

type netBlock struct {
	string
	colorful.Color
}

func isBluey(l, a, b float64) bool {
	h, c, L := colorful.LabToHcl(l, a, b)
	return 195 < h && h < 255 && 0.2 < c && c < 1.0 && 0 < L // && L < 1.20
}

func isGreeny(l, a, b float64) bool {
	h, c, L := colorful.LabToHcl(l, a, b)
	return 125 < h && h < 175 && 0.2 < c && c < 1.0 && 0 < L && L < .7
}

func isBrowny(l, a, b float64) bool {
	h, c, L := colorful.LabToHcl(l, a, b)
	return 45 < h && h < 100 && 0.2 < c && c < .7 && 0 < L && L < .6
}

func setupEightBlocks() {
	rand.Seed(1)
	Sps := colorful.SoftPaletteSettings{
		Iterations:  50,
		ManySamples: true,
	}

	switch MapColor.Theme {
	case Bluey:
		Sps.CheckColor = isBluey
	case Greeny:
		Sps.CheckColor = isGreeny
	case Browny:
		Sps.CheckColor = isBrowny
	}
	pallet, err := colorful.SoftPaletteEx(11, Sps)
	if err != nil {
		panic(err)
	}

	cPriv := pallet[0]
	cMulticast := pallet[1]
	cReserv := pallet[2]
	cApnic := pallet[3]
	cRipe := pallet[4]
	cArin := pallet[5]
	cJpnic := pallet[6]
	cAfrinic := pallet[7]
	cLacnic := pallet[8]
	cDOD := pallet[9]
	cOther := pallet[10]

	nbMulticast := netBlock{"Multicast", cMulticast}
	nbReserv := netBlock{"Reserved", cReserv}
	nbApnic := netBlock{"APNIC", cApnic}
	nbRipe := netBlock{"RIPE", cRipe}
	nbArin := netBlock{"ARIN", cArin}
	nbJpnic := netBlock{"JPNIC", cJpnic}
	nbAfrinic := netBlock{"AFRINIC", cAfrinic}
	nbLacnic := netBlock{"LACNIC", cLacnic}

	eightBlock = map[int]netBlock{
		0:   netBlock{"Local ID", cPriv},
		10:  netBlock{"Private", cPriv},
		127: netBlock{"Loopback", cPriv},
		192: netBlock{"IANA", cPriv},
		224: nbMulticast, 225: nbMulticast, 226: nbMulticast, 227: nbMulticast,
		228: nbMulticast, 229: nbMulticast, 230: nbMulticast, 231: nbMulticast,
		232: nbMulticast, 233: nbMulticast, 234: nbMulticast, 235: nbMulticast,
		236: nbMulticast, 237: nbMulticast, 238: nbMulticast, 239: nbMulticast,
		240: nbReserv, 241: nbReserv, 242: nbReserv, 243: nbReserv,
		244: nbReserv, 245: nbReserv, 246: nbReserv, 247: nbReserv,
		248: nbReserv, 249: nbReserv, 250: nbReserv, 251: nbReserv,
		252: nbReserv, 253: nbReserv, 254: nbReserv, 255: nbReserv,

		1: nbApnic, 14: nbApnic, 27: nbApnic, 36: nbApnic, 39: nbApnic,
		42: nbApnic, 49: nbApnic, 58: nbApnic, 59: nbApnic, 60: nbApnic,
		61: nbApnic, 101: nbApnic, 103: nbApnic, 106: nbApnic, 110: nbApnic,
		111: nbApnic, 112: nbApnic, 113: nbApnic, 114: nbApnic, 115: nbApnic,
		116: nbApnic, 117: nbApnic, 118: nbApnic, 119: nbApnic, 120: nbApnic,
		121: nbApnic, 122: nbApnic, 123: nbApnic, 124: nbApnic, 125: nbApnic,
		126: nbApnic, 133: nbApnic, 150: nbApnic, 153: nbApnic, 163: nbApnic,
		171: nbApnic, 175: nbApnic, 180: nbApnic, 182: nbApnic, 183: nbApnic,
		202: nbApnic, 203: nbApnic, 210: nbApnic, 211: nbApnic, 218: nbApnic,
		219: nbApnic, 220: nbApnic, 221: nbApnic, 222: nbApnic, 223: nbApnic,

		2: nbRipe, 5: nbRipe, 31: nbRipe, 37: nbRipe, 46: nbRipe,
		51: nbRipe, 62: nbRipe, 57: nbRipe, 77: nbRipe, 78: nbRipe,
		79: nbRipe, 80: nbRipe, 81: nbRipe, 82: nbRipe, 83: nbRipe,
		84: nbRipe, 85: nbRipe, 86: nbRipe, 87: nbRipe, 88: nbRipe,
		89: nbRipe, 90: nbRipe, 91: nbRipe, 92: nbRipe, 93: nbRipe,
		94: nbRipe, 95: nbRipe, 109: nbRipe, 141: nbRipe, 145: nbRipe,
		151: nbRipe, 176: nbRipe, 178: nbRipe, 185: nbRipe, 188: nbRipe,
		193: nbRipe, 194: nbRipe, 195: nbRipe, 212: nbRipe, 213: nbRipe,
		217: nbRipe,

		23: nbArin, 24: nbArin, 35: nbArin, 40: nbArin, 45: nbArin,
		50: nbArin, 52: nbArin, 54: nbArin, 63: nbArin, 64: nbArin,
		66: nbArin, 67: nbArin, 68: nbArin, 69: nbArin, 70: nbArin,
		71: nbArin, 72: nbArin, 73: nbArin, 74: nbArin, 75: nbArin,
		76: nbArin, 96: nbArin, 97: nbArin, 98: nbArin, 99: nbArin,
		100: nbArin, 104: nbArin, 107: nbArin, 108: nbArin, 128: nbArin,
		129: nbArin, 130: nbArin, 131: nbArin, 132: nbArin, 134: nbArin,
		135: nbArin, 136: nbArin, 137: nbArin, 138: nbArin, 139: nbArin,
		140: nbArin, 143: nbArin, 142: nbArin, 144: nbArin, 146: nbArin,
		147: nbArin, 148: nbArin, 149: nbArin, 152: nbArin, 155: nbArin,
		156: nbArin, 157: nbArin, 158: nbArin, 159: nbArin, 160: nbArin,
		161: nbArin, 162: nbArin, 164: nbArin, 165: nbArin, 166: nbArin,
		167: nbArin, 168: nbArin, 169: nbArin, 170: nbArin, 172: nbArin,
		173: nbArin, 174: nbArin, 184: nbArin, 198: nbArin, 199: nbArin,
		204: nbArin, 205: nbArin, 206: nbArin, 207: nbArin, 208: nbArin,
		209: nbArin, 216: nbArin,

		43: nbJpnic,

		41: nbAfrinic, 102: nbAfrinic, 105: nbAfrinic, 154: nbAfrinic, 196: nbAfrinic,
		197: nbAfrinic,

		177: nbLacnic, 179: nbLacnic, 181: nbLacnic, 186: nbLacnic, 187: nbLacnic,
		189: nbLacnic, 190: nbLacnic, 191: nbLacnic, 200: nbLacnic, 201: nbLacnic,

		6: netBlock{"DoD ArmyIS", cDOD}, 7: netBlock{"DoD DNIC", cDOD}, 11: netBlock{"DoD IIS", cDOD},
		21: netBlock{"DoD DDN", cDOD}, 22: netBlock{"DoD DISNET", cDOD}, 26: netBlock{"DoD MILNET", cDOD},
		28: netBlock{"DoD DSI-N", cDOD}, 29: netBlock{"DoD MILX25", cDOD}, 30: netBlock{"DoD ARPAX25", cDOD},
		33: netBlock{"DoD DCMC", cDOD}, 55: netBlock{"CONUS RCAS2", cDOD}, 214: netBlock{"DoD DNIC", cDOD},
		215: netBlock{"DoD DNIC", cDOD},

		4: netBlock{"L3", cOther}, 8: netBlock{"L3", cOther},

		12: netBlock{"ATT", cOther}, 32: netBlock{"ATT", cOther},
		47: netBlock{"BellNorth", cOther}, 65: netBlock{"BellSouth", cOther},

		15: netBlock{"HP", cOther}, 16: netBlock{"HP", cOther},

		3:  netBlock{"GE", cOther},
		9:  netBlock{"IBM", cOther},
		13: netBlock{"Xerox", cOther},
		17: netBlock{"Apple", cOther},
		18: netBlock{"MIT", cOther},
		19: netBlock{"Ford", cOther},
		20: netBlock{"CEC", cOther},
		25: netBlock{"UK MoD", cOther},
		34: netBlock{"Haliburton", cOther},
		38: netBlock{"Cogent", cOther},
		44: netBlock{"AMPRNet", cOther},
		48: netBlock{"Prudential", cOther},
		53: netBlock{"Daimler", cOther},
		56: netBlock{"USPS", cOther},
	}
}

//InitMapImg sets up image for population.
func InitMapImg(px int) error {
	// Create White Canvas
	m := image.NewRGBA(image.Rect(0, 0, px, px))
	draw.Draw(m, m.Bounds(), image.Black, image.ZP, draw.Src)

	// Generate Font
	f, err := getFont("luxisr.ttf")
	if err != nil {
		return fmt.Errorf("InitMapImg: Could not get font: %s", err.Error())
	}
	ftc := freetype.NewContext()
	ftc.SetDPI(60)
	ftc.SetFont(f)
	ftc.SetFontSize(12)
	ftc.SetSrc(image.Black)
	ftc.SetDst(m)

	netMapImg.RGBA = m
	netMapImg.Context = ftc
	return nil
}

//InitMap initializes netmap.Block with IANA data, and cell colors.
func (n netMap) InitMap(px int) error {
	err := InitMapImg(px)
	if err != nil {
		return fmt.Errorf("InitMap: Could not initialize map image: %s", err.Error())
	}

	setupEightBlocks()
	grid := 16
	size := px / grid
	s, _ := hilbert.NewHilbert(grid)
	for i := 0; i <= grid*grid-1; i++ {
		x, y, _ := s.Map(i)
		Block = append(Block, NetCell{
			Name: fmt.Sprintf("%d.0.0.0", i),
			X:    x,
			Y:    y,
			Pos:  i,
			Cell: image.Rect(x*size, y*size, x*size+size-1, y*size+size-1),
		})
		_, named := eightBlock[i]
		if named {
			Block[i].Label = eightBlock[i].string
			Block[i].Fill = eightBlock[i].Color
			if MapColor.Whole {
				Block[i].Fill = MapColor.Color
			}
		}
	}
	return nil
}

func drawMap() {
	for _, c := range Block {
		// drawBorder(netMapImg.RGBA, c.Cell, color.RGBA{0, 0, 0, 255})
		drawFill(netMapImg.RGBA, c.Cell, c.Fill)
		drawText(netMapImg.RGBA, c.Cell, netMapImg.Context, c.Name, 1)
		drawText(netMapImg.RGBA, c.Cell, netMapImg.Context, c.Label, 2)
		drawText(netMapImg.RGBA, c.Cell, netMapImg.Context, c.Info, 3)
	}
}

// Read Font
func getFont(name string) (*truetype.Font, error) {
	if FontLocation == "" {
		return nil, fmt.Errorf("getFont: Specify font before initializing")
	}
	fontBytes, err := ioutil.ReadFile(FontLocation)
	if err != nil {
		return nil, fmt.Errorf("getFont: Could not read font: %s", err.Error())
	}
	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		return nil, fmt.Errorf("getFont: Could not parse font: %s", err.Error())
	}
	return f, nil
}

//WriteFile writes the image to file fn.
func WriteFile(fn string) {
	drawMap()
	w, err := os.Create(fn)
	if err != nil {
		panic(err)
	}
	defer w.Close()
	png.Encode(w, netMapImg.RGBA)
}

//Write returns a bytes.Buffer containing the image.
func Write() *bytes.Buffer {
	drawMap()
	bytebuf := bytes.NewBufferString("")
	png.Encode(bytebuf, netMapImg.RGBA)
	return bytebuf
}

//Base64 returns the image as a base64 encoded string.
func Base64() string {
	img := Write()
	str := base64.StdEncoding.EncodeToString(img.Bytes())
	return str
}

func drawText(m *image.RGBA, r image.Rectangle, ftc *freetype.Context, s string, line int) {
	x, y := r.Bounds().Min.X, r.Bounds().Min.Y
	fac := int(ftc.PointToFixed(12) >> 6)
	ftc.SetClip(r)
	ss := strings.Split(s, "\n")
	for _, st := range ss {
		pt := freetype.Pt(x-9+fac, y+(line*fac))
		ftc.DrawString(st, pt)
		line++
	}
}

func drawFill(m *image.RGBA, r image.Rectangle, c colorful.Color) {
	draw.Draw(m, r, &image.Uniform{c}, image.ZP, draw.Src)
}
