package inetmap

import (
	"image"
	"image/color"
	"image/draw"
	"testing"
)

func setupImg() *image.RGBA {
	m := image.NewRGBA(image.Rect(0, 0, 300, 100))
	draw.Draw(m, m.Bounds(), image.White, image.ZP, draw.Src)
	return m
}

func BenchmarkDrawFill(b *testing.B) {
	m := setupImg()
	for n := 0; n < b.N; n++ {
		drawFill(m, m.Bounds(), color.RGBA{255, 255, 255, 255})
	}
}

func BenchmarkDrawBorder(b *testing.B) {
	m := setupImg()
	for n := 0; n < b.N; n++ {
		drawBorder(m, m.Bounds(), color.RGBA{255, 255, 255, 255})
	}
}
